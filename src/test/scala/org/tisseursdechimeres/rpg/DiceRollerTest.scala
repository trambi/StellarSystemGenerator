package org.tisseursdechimeres.rpg

import org.scalatest.FlatSpec

/**
  * Created by Trambi on 28/11/2016.
  */
class DiceRollerTest extends FlatSpec {
  "A normal roll 100 (fixed roll at 54)" should "return 54" in {
    val gen = new FixedIntegerGenerator
    val diceRoller = new DiceRoller
    gen.addFixedInteger(54)
    diceRoller.setIntegerGenerator(gen)
    assert( 54 == diceRoller.roll100() )
  }

  "A normal roll 100 (fixed roll at 98)" should "return 98" in {
    val gen = new FixedIntegerGenerator
    val diceRoller = new DiceRoller
    gen.addFixedInteger(98)
    diceRoller.setIntegerGenerator(gen)
    assert( 98 == diceRoller.roll100() )
  }

  "An open ended high roll 100 (fixed roll at 38)" should "return 38" in {
    val gen = new FixedIntegerGenerator
    val diceRoller = new DiceRoller
    gen.addFixedInteger(38)
    diceRoller.setIntegerGenerator(gen)
    assert( 38 == diceRoller.openEndedHighRoll100() )
  }

  "An open ended high roll 100 (fixed roll at 96 then 54)" should "return 150" in {
    val gen = new FixedIntegerGenerator
    val diceRoller = new DiceRoller
    gen.addFixedInteger(96)
    gen.addFixedInteger(54)
    diceRoller.setIntegerGenerator(gen)
    assert( 150 == diceRoller.openEndedHighRoll100() )
  }

  "An open ended high roll 100 (fixed roll at 96 then 97 then 27)" should "return 220" in {
    val gen = new FixedIntegerGenerator
    val diceRoller = new DiceRoller
    gen.addFixedInteger(96)
    gen.addFixedInteger(97)
    gen.addFixedInteger(27)
    diceRoller.setIntegerGenerator(gen)
    assert( 220 == diceRoller.openEndedHighRoll100() )
  }

  "An open ended roll 100 (fixed roll at 38)" should "return 38" in {
    val gen = new FixedIntegerGenerator
    val diceRoller = new DiceRoller
    gen.addFixedInteger(38)
    diceRoller.setIntegerGenerator(gen)
    assert( 38 == diceRoller.openEndedRoll100() )
  }

  "An open ended roll 100 (fixed roll at 96 then 54)" should "return 150" in {
    val gen = new FixedIntegerGenerator
    val diceRoller = new DiceRoller
    gen.addFixedInteger(96)
    gen.addFixedInteger(54)
    diceRoller.setIntegerGenerator(gen)
    assert( 150 == diceRoller.openEndedRoll100() )
  }

  "An open ended roll 100 (fixed roll at 96 then 97 then 27)" should "return 220" in {
    val gen = new FixedIntegerGenerator
    val diceRoller = new DiceRoller
    gen.addFixedInteger(96)
    gen.addFixedInteger(97)
    gen.addFixedInteger(27)
    diceRoller.setIntegerGenerator(gen)
    assert( 220 == diceRoller.openEndedRoll100() )
  }

  "An open ended roll 100 (fixed roll at 4 then 5)" should "return -1" in {
    val gen = new FixedIntegerGenerator
    val diceRoller = new DiceRoller
    gen.addFixedInteger(4)
    gen.addFixedInteger(5)
    diceRoller.setIntegerGenerator(gen)
    assert( -1 == diceRoller.openEndedRoll100() )
  }

  "An open ended roll 100 (fixed roll at 4 then 3)" should "return 1" in {
    val gen = new FixedIntegerGenerator
    val diceRoller = new DiceRoller
    gen.addFixedInteger(4)
    gen.addFixedInteger(3)
    diceRoller.setIntegerGenerator(gen)
    assert( 1 == diceRoller.openEndedRoll100() )
  }

  "An open ended roll 100 (fixed roll at 4 then 97 then 57)" should "return -150" in {
    val gen = new FixedIntegerGenerator
    val diceRoller = new DiceRoller
    gen.addFixedInteger(4)
    gen.addFixedInteger(97)
    gen.addFixedInteger(57)
    diceRoller.setIntegerGenerator(gen)
    assert( -150 == diceRoller.openEndedRoll100() )
  }

}
