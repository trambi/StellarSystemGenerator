package org.tisseursdechimeres.rpg

/**
  * Created by Trambi on 28/11/2016.
  */
class FixedIntegerGenerator extends IntegerGenerator {
  var integerQueue = scala.collection.immutable.Queue[Int]()

  def addFixedInteger(fixedC:Int) : Unit = {
    integerQueue = integerQueue.enqueue(fixedC)
  }

  override def get(max: Int) :Int = {
    val (result, newQueue ) = integerQueue.dequeue
    integerQueue = newQueue
    result
  }
}
