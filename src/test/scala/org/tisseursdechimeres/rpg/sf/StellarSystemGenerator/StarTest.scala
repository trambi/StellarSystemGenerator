package org.tisseursdechimeres.rpg.sf.StellarSystemGenerator

import org.scalatest.FlatSpec

/**
  * Created by Trambi on 29/01/2016.
  */
class StarTest  extends FlatSpec{

  "Uninitialized star" should "have empty name" in {
    val uninitializedStar = new Star
    assert( "" == uninitializedStar.name )
  }

  "I StellarType" should "have I code " in {
    val star:Star = new Star
    star.setStellarType("I")
    assert(  "I" == star.stellarType)
  }
  "II StellarType" should "have II code " in {
    val star:Star = new Star
    star.setStellarType("II")
    assert(  "II" == star.stellarType)
  }
  "III StellarType" should "have III code " in {
    val star:Star = new Star
    star.setStellarType("III")
    assert(  "III" == star.stellarType)
  }
  "IV StellarType" should "have IV code " in {
    val star:Star = new Star
    star.setStellarType("IV")
    assert(  "IV" == star.stellarType)
  }
  "V StellarType" should "have V code " in {
    val star:Star = new Star
    star.setStellarType("V")
    assert(  "V" == star.stellarType)
  }

  "Star" must "compute scientific type" in {
    var star = new Star
    star.setSpectralClass("F")
    star.setRelativeBrightness(2)
    star.setStellarType("V")
    assert( "F2 V" == star.scientificType() )
  }

  "Star" must "have mass in Sun mass" in {
    var star = new Star
    star.mass = 1.00
    assert( 1 == star.mass )
    assert( star.solarMassInKg.toDouble === star.massInKg() )
  }

  "Star" must "have mass in Sun radius" in {
    var star = new Star
    star.radius = 1.00
    assert( 1.00 == star.radius )
    assert( star.solarRadiusInMeter.toDouble == star.radiusInMeter() )
  }

}
