package org.tisseursdechimeres.rpg.sf.StellarSystemGenerator

import org.tisseursdechimeres.rpg.FixedIntegerGenerator

import org.scalatest.FlatSpec

class StarGeneratorTest extends FlatSpec{
  "StarGenerator roll 50 then 5 " should "give a VM star" in {
    val gen = new FixedIntegerGenerator
    gen.addFixedInteger(50)
    gen.addFixedInteger(5)
    gen.addFixedInteger(5)
    StarGenerator.setIntegerGenerator(gen)
    val core = StarGenerator()
    assert( 1 == core.length )
    assert( core(0).isInstanceOf[Star] )
    val star = core(0).asInstanceOf[Star]
    assert( "V" == star.stellarType() )
    assert( "M" == star.spectralClass() )
  }

  "StarGenerator roll 76 then 5 " should "give a VK star" in {
    val gen = new FixedIntegerGenerator
    gen.addFixedInteger(76)
    gen.addFixedInteger(5)
    gen.addFixedInteger(5)
    StarGenerator.setIntegerGenerator(gen)
    val core = StarGenerator()
    assert( 1 == core.length )
    assert( core(0).isInstanceOf[Star] )
    val star = core(0).asInstanceOf[Star]
    assert( "V" == star.stellarType() )
    assert( "K" == star.spectralClass() )
  }

  "StarGenerator roll 85 then 5 " should "give a VG star" in {
    val gen = new FixedIntegerGenerator
    gen.addFixedInteger(85)
    gen.addFixedInteger(5)
    gen.addFixedInteger(5)
    StarGenerator.setIntegerGenerator(gen)
    val core = StarGenerator()
    assert( 1 == core.length )
    assert( core(0).isInstanceOf[Star] )
    val star = core(0).asInstanceOf[Star]
    assert( "V" == star.stellarType() )
    assert( "G" == star.spectralClass() )
  }

  "StarGenerator roll 90 then 5 " should "give a VF star" in {
    val gen = new FixedIntegerGenerator
    gen.addFixedInteger(90)
    gen.addFixedInteger(5)
    gen.addFixedInteger(5)
    StarGenerator.setIntegerGenerator(gen)
    val core = StarGenerator()
    assert( 1 == core.length )
    assert( core(0).isInstanceOf[Star] )
    val star = core(0).asInstanceOf[Star]
    assert( "V" == star.stellarType() )
    assert( "F" == star.spectralClass() )
  }

  "StarGenerator roll 95 then 5 " should "give a VA star" in {
    val gen = new FixedIntegerGenerator
    gen.addFixedInteger(95)
    gen.addFixedInteger(5)
    gen.addFixedInteger(5)
    StarGenerator.setIntegerGenerator(gen)
    val core = StarGenerator()
    assert( 1 == core.length )
    assert( core(0).isInstanceOf[Star] )
    val star = core(0).asInstanceOf[Star]
    assert( "V" == star.stellarType() )
    assert( "A" == star.spectralClass() )
  }

  "StarGenerator roll 96 then 25 then 5 " should "give a VB star" in {
    val gen = new FixedIntegerGenerator
    gen.addFixedInteger(96)
    gen.addFixedInteger(25)
    gen.addFixedInteger(5)
    gen.addFixedInteger(5)
    StarGenerator.setIntegerGenerator(gen)
    val core = StarGenerator()
    assert( 1 == core.length )
    assert( core(0).isInstanceOf[Star] )
    val star = core(0).asInstanceOf[Star]
    assert( "V" == star.stellarType() )
    assert( "B" == star.spectralClass() )
  }

  "StarGenerator roll 96 then 50 then 5 " should "give a VO star" in {
    val gen = new FixedIntegerGenerator
    gen.addFixedInteger(96)
    gen.addFixedInteger(50)
    gen.addFixedInteger(5)
    gen.addFixedInteger(5)
    StarGenerator.setIntegerGenerator(gen)
    val core = StarGenerator()
    assert( 1 == core.length )
    assert( core(0).isInstanceOf[Star] )
    val star = core(0).asInstanceOf[Star]
    assert( "V" == star.stellarType() )
    assert( "O" == star.spectralClass() )
  }

  "StarGenerator roll 96 then 70 then 5 " should "give a VN star" in {
    val gen = new FixedIntegerGenerator
    gen.addFixedInteger(96)
    gen.addFixedInteger(70)
    gen.addFixedInteger(5)
    gen.addFixedInteger(5)
    StarGenerator.setIntegerGenerator(gen)
    val core = StarGenerator()
    assert( 1 == core.length )
    assert( core(0).isInstanceOf[Star] )
    val star = core(0).asInstanceOf[Star]
    assert( "V" == star.stellarType() )
    assert( "N" == star.spectralClass() )
  }

  "StarGenerator roll 96 then 90 then 5 " should "give a VS star" in {
    val gen = new FixedIntegerGenerator
    gen.addFixedInteger(96)
    gen.addFixedInteger(90)
    gen.addFixedInteger(5)
    gen.addFixedInteger(5)
    StarGenerator.setIntegerGenerator(gen)
    val core = StarGenerator()
    assert( 1 == core.length )
    assert( core(0).isInstanceOf[Star] )
    val star = core(0).asInstanceOf[Star]
    assert( "V" == star.stellarType() )
    assert( "S" == star.spectralClass() )
  }

  "StarGenerator roll 96 then 98 then 50 " should "give a phenomenom" in {
    val gen = new FixedIntegerGenerator
    gen.addFixedInteger(96)
    gen.addFixedInteger(98)
    gen.addFixedInteger(50)
    gen.addFixedInteger(5)
    StarGenerator.setIntegerGenerator(gen)
    val core = StarGenerator()
    assert( 1 == core.length )
    assert( core(0).isInstanceOf[StellarPhenomenom] )
  }
}
