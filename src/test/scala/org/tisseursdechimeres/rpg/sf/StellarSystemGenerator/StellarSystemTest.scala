package org.tisseursdechimeres.rpg.sf.StellarSystemGenerator

import org.scalatest.FlatSpec

class StellarSystemTest extends FlatSpec{
  "Uninitialized stellar system " should "have an empty name" in {
    val stellarSystem = new StellarSystem()
    assert( "" == stellarSystem.name )
  }
}
