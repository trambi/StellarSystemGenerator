package org.tisseursdechimeres.rpg.sf.StellarSystemGenerator

abstract class StellarBody() {
  var name :String =""
  var mass :Double = 0.0 //in the solar mass ie 1.98855 * 10 ^ 30 kg
  var radius :Double = 0.0 //in Solar radius ie  6.957 * 10 ^ 8 m
  val solarMassInKg = 1.98855 * Math.pow(10,30)
  val solarRadiusInMeter = 6.957 * Math.pow(10,8)

  def massInKg(): Double={
    mass * solarMassInKg
  }
  def radiusInMeter(): Double={
    radius * solarRadiusInMeter
  }
}
