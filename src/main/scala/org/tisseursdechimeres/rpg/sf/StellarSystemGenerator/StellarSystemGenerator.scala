package org.tisseursdechimeres.rpg.sf.StellarSystemGenerator

class StellarSystemGenerator(systemNameToGenerate:String = "") {
  private var _system = new StellarSystem(systemNameToGenerate)
  def apply(): StellarSystem ={
    generateCore()
    _system
  }

  def generateCore(): StellarSystemGenerator = {
    _system.addStellarBodies (StarGenerator())
    this
  }

}

object StellarSystemGenerator {


  def main(args: Array[String]): Unit = {
    println("Welcome in StellarSystemGenerator")
    var generator = new StellarSystemGenerator()
    val system = generator()
    println("System: " + system.toString())
  }

}
