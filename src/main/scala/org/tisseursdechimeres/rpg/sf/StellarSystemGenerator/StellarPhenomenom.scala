package org.tisseursdechimeres.rpg.sf.StellarSystemGenerator

class StellarPhenomenom extends StellarBody{
  override def toString(): String={
    "Phenomenom " + name + " mass: " + mass + " radius: " + radius
  }
}

