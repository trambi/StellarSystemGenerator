package org.tisseursdechimeres.rpg.sf.StellarSystemGenerator

import scala.Array._

/**
  * Created by Trambi on 28/01/2016.
  */
class StellarSystem(nameC:String = "") {
  var name = nameC
  private var stellarBodies: Array[StellarBody] = empty[StellarBody]
  def addStellarBody(stellarBody: StellarBody): StellarSystem ={
    stellarBodies :+ stellarBody
    this
  }

  def addStellarBodies(bodies: Array[StellarBody]): StellarSystem ={
    stellarBodies = stellarBodies ++ bodies
    this
  }

  def getStellarBodies(): Array[StellarBody] ={
    stellarBodies
  }

  override def toString(): String ={
    stellarBodies.mkString("\n")
  }

}
