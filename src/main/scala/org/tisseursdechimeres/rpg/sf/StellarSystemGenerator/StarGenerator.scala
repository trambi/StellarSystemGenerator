package org.tisseursdechimeres.rpg.sf.StellarSystemGenerator

import org.tisseursdechimeres.rpg.DiceRoller
import org.tisseursdechimeres.rpg.IntegerGenerator

object StarGenerator {
  private var dice = new DiceRoller()

  def setIntegerGenerator(integerGenerator:IntegerGenerator) : Unit = {
    dice.setIntegerGenerator(integerGenerator)
  }

  def apply(): Array[StellarBody] = {
    val core = spectralClasses()
    core.map( _ match {
        case star: Star => generateStar(star)
        case phenomenom: StellarPhenomenom => phenomenom
      }
    )
  }

  private def spectralClass(): StellarBody = {
    val roll = dice.openEndedHighRoll100()
    //println("spectralClass roll " + roll.toString())
    var core: StellarBody = null
    if( 76 > roll ){
      core  = new Star().setSpectralClass("M")
    }else if ( 82 > roll ){
      core  = new Star().setSpectralClass("K")
    }else if ( 87 > roll ){
      core = new Star().setSpectralClass("G")
    }else if ( 92 > roll ){
      core = new Star().setSpectralClass("F")
    }else if ( 96 > roll ){
      core = new Star().setSpectralClass("A")
    }else if ( 121 > roll ){
      core = new Star().setSpectralClass("B")
    }else if ( 141 > roll ){
      core = new Star().setSpectralClass("O")
    }else if ( 161 > roll ){
      core = new Star().setSpectralClass("N")
    }else if ( 181 > roll ){
      core = new Star().setSpectralClass("S")
    }else{
      core = new StellarPhenomenom()
    }
    //println(core)
    core
  }

  private def spectralClasses(): Array[StellarBody] = {
    var core :Array[StellarBody] = null
    val roll = dice.openEndedHighRoll100()
    //println(">spectralClasses roll " + roll.toString())
    if( 76 > roll ){
      core = Array( new Star().setSpectralClass("M"))
    }else if ( 82 > roll ){
      core = Array( new Star().setSpectralClass("K") )
    }else if ( 87 > roll ){
      core = Array( new Star().setSpectralClass("G") )
    }else if ( 92 > roll ){
      core = Array( new Star().setSpectralClass("F") )
    }else if ( 96 > roll ){
      core = Array( new Star().setSpectralClass("A") )
    }else if ( 121 > roll ){
      // Arf we roll a cluster let's have some hard time
      val numberRoll = dice.get(4) + 1
      core = Array[Int](numberRoll).map( whatever => spectralClass() )
    }else if ( 141 > roll ){
      core = Array( new Star().setSpectralClass("B") )
    }else if ( 161 > roll ){
      core = Array( new Star().setSpectralClass("O") )
    }else if ( 181 > roll ){
      core = Array( new Star().setSpectralClass("N") )
    }else if ( 201 > roll ){
      core = Array( new Star().setSpectralClass("S") )
    }else{
      core = Array( new StellarPhenomenom() )
    }
    //println("<spectralClasses: "+ core.mkString("\n"))
    core
  }

  private def generateStar(star: Star) : Star = {
    brightness(stellarType(star))
  }

  private def brightness(star: Star) : Star = {
    val roll = dice.roll10()
    star.setRelativeBrightness(roll - 1)
  }

  private def stellarTypeOfKAndM(star: Star) : Star = {
    val roll = dice.get(9)
    //println("stellarType roll " + roll.toString())
    if(7 == roll){
      star.setStellarType("III")
    }else if (8 == roll){
      star.setStellarType("II")
    }else if (9 == roll){
      star.setStellarType("I")
    }else{
      star.setStellarType("V")
    }
    star
  }

  private def stellarTypeBeyondG(star: Star) : Star = {
    val roll = dice.roll10()
    //println("stellarType roll " + roll.toString())
    if(7 == roll ){
      star.setStellarType("IV")
    }else if(8 == roll){
      star.setStellarType("III")
    }else if (9 == roll){
      star.setStellarType("II")
    }else if (10 == roll){
      star.setStellarType("I")
    }else{
      star.setStellarType("V")
    }
    return star
  }

  private def stellarType(star:Star) : Star = {
    var tempStar = star
    if ( "M" == star.spectralClass || "K"  == star.spectralClass ){
      tempStar = stellarTypeOfKAndM(star)
    }else{
      tempStar = stellarTypeBeyondG(star)
    }
    //println("return of stellarType: " + tempStar)
    tempStar
  }

}
