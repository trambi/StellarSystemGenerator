package org.tisseursdechimeres.rpg.sf.StellarSystemGenerator

class Star extends StellarBody{
  private var _spectralClass: String = ""
  private var _relativeBrightness: Int = -1
  private var _stellarType: String = ""

  def scientificType():String={
    _spectralClass + _relativeBrightness.toString + " " + _stellarType
  }

  val stellarTypesByCode = Map[String,String](
    ("V","Main sequence star"),
    ("IV","Sub-giant"),
    ("III", "Giant"),
    ("II", "Bright"),
    ("I","Super-giant") )

  def setStellarType(code: String): Star ={
    if (stellarTypesByCode.isDefinedAt(code)){
      _stellarType = code
    }
    this
  }

  def stellarType(): String={
    _stellarType
  }

  def stellarTypeDescription(): String={
    stellarTypesByCode(_stellarType)
  }

  val spectralClassesByCode = Map[String,String](
    ("M","Bright red: toward orange at the hot red"),
    ("K","Orange; from yellowish to reddish"),
    ("G", "Yellow"),
    ("F","White"),
    ("A", "Blue-White"),
    ("B", "Very bright blue-white"),
    ("O","Extremely bright blue-white"),
    ("N","Deep to dull red"),
    ("S","Dim red") )

  def setSpectralClass(code: String): Star ={
    if (spectralClassesByCode.isDefinedAt(code)){
      _spectralClass = code
    }
    this
  }

  def spectralClass(): String={
    _spectralClass
  }

  def spectralClassDescription(): String={
    spectralClassesByCode(_spectralClass)
  }

  override def toString(): String={
    "Star " + name + " " + scientificType() + " mass: " + mass + " radius: " + radius
  }

  def setRelativeBrightness(brightness: Int): Star={
    _relativeBrightness = brightness
    computeMass()
  }

  /**
   This is the Hertzprung-Russell show !!
  */
  private def computeMass(): Star={

    this
  }
}
