package org.tisseursdechimeres.rpg

/**
  * Created by Trambi on 28/11/2016.
  */
class DiceRoller {
  var integerGenerator:IntegerGenerator = new RandomIntegerGenerator

  def setIntegerGenerator(integerGeneratorC:IntegerGenerator) : Unit = {
    integerGenerator = integerGeneratorC
  }

  def get(max:Int) : Int ={
    integerGenerator.get(max)
  }

  def roll100() : Int ={
    integerGenerator.get(100)
  }

  def roll10() : Int ={
    integerGenerator.get(10)
  }

  def openEndedRoll100() : Int ={
    var roll = roll100
    if( 95 < roll ){
      roll = roll + openEndedHighRoll100
    }else if ( 5 > roll ){
      roll = roll - openEndedHighRoll100
    }
    return roll
  }

  def openEndedHighRoll100() : Int ={
    var roll = roll100
    if( 95 < roll ){
      roll = roll + openEndedHighRoll100
    }
    return roll
  }

}
