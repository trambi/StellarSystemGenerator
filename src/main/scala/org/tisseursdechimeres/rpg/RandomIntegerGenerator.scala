package org.tisseursdechimeres.rpg
import scala.util.Random

/**
  * Created by Trambi on 28/11/2016.
  */
class RandomIntegerGenerator extends IntegerGenerator {
  val random = new Random(System.currentTimeMillis)

  override def get(max: Int):Int ={
    random.nextInt(max-1)+1
  }
}
