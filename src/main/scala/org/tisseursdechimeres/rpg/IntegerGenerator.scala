package org.tisseursdechimeres.rpg

/**
  * Created by Trambi on 28/11/2016.
  */
trait IntegerGenerator {
  def get(max: Int):Int
}
